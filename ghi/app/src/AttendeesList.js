function AttendeesList(props) {

  // const handleDeleteButton = async (e) => {
  //   const { id } = e.target;
  //   const resp = await fetch(`http://localhost:8001/api/attendees/${id}`,{
  //       method: "delete"
  //   })

  //   if(resp.ok) {
  //     const data = await resp.json();
  //     setAttendees(attendees.filter(l => (l.id !== parseInt(id))))
  //   }}

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {props.attendees.map((attendee) => {
          return (
            <tr key={attendee.href}>
              <td>{attendee.name}</td>
              <td>{attendee.conference}</td>
              {/* <td><button className="btn btn-danger" id={location.id} onClick={handleDeleteButton}>Delete</button></td> */}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AttendeesList;
