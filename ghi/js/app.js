function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col">
    <div class="card" style="box-shadow: 5px 5px 5px 5px rgba(0,0,0,0.2); margin: 20px">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-body-secondary">${location}</p>
        <p class="card-text">${description}</p>
        <div class="card-footer">
        <p class="card-text">${startDate} - ${endDate}</p>

        </div>

        </div>
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
    const response = await fetch(url);

    if (!response.ok) {
        // Figure out what to do when the response is bad
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            //conference start date and end date
            const startDate = details.conference.starts;
            const start = new Date(startDate);
            let startString = start.toLocaleDateString();

            const endDate = details.conference.ends;
            const end = new Date(endDate);
            let endString = end.toLocaleDateString();

            const location = details.conference.location.name;

            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const row = document.querySelector('.row');
            row.innerHTML += html;


        }
        }

    }
    } catch (e) {
      console.error('error', error);
    }

});
